package weiyuan.javatutorial.cuncurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;


/**
 * Created by Weiyuan Zhu on 08/12/2016.
 * Test Future<V>
 */

public final class FutureObjectTest {
    private FutureObjectTest() { }


    public static void main(final String[] args) {

        ExecutorService exec;
        exec = Executors.newCachedThreadPool();

        List<Future<String>> results = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
             results.add(exec.submit(new FutureTest(i)));
        }


        try {
            for (Future<String> fs : results) {
                System.out.println("Result id: " + fs.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            exec.shutdown();
        }
    }

    static class FutureTest implements Callable<String> {

        private int id;

        FutureTest(final int number) {
            this.id = number;
        }

        @Override
        public String call() throws Exception {
            return String.valueOf(this.id);
        }
    }
}
