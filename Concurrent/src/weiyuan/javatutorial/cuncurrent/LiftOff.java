package weiyuan.javatutorial.cuncurrent;

/**
 * Created by Weiyuan Zhu on 16/12/2016.
 */
public class LiftOff {

    private int countDown = 10;
    private static int taskCount = 0;
    private final int id = taskCount++;

    public LiftOff() { }

    public LiftOff(final int countDown) {
        this.countDown = countDown;
    }

}
