package weiyuan.javatutorial.cuncurrent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Weiyuan Zhu on 09/01/2017.
 * ScheduledExecutorService tutorial
 * Run some tasks at a planned time
 * Lambda expression introduced
 */
public final class ScheduledExecutorServiceTest {
    private ScheduledExecutorServiceTest() { }

    public static void main(final String[] args) {
        Runnable beeper = () -> System.out.println("beep");

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        ScheduledFuture beepHandler = scheduler.scheduleAtFixedRate(beeper, 3, 3, TimeUnit.SECONDS);

        scheduler.schedule(
                () -> {
                    beepHandler.cancel(true);
                    scheduler.shutdown();
                }, 15, TimeUnit.SECONDS);


    }
}
