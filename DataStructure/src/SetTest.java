import java.util.*;

public class SetTest {

    public static void main(String[] args) {
        Map<Integer,String> testMap = new HashMap<>();
        testMap.put(10,"10mA");
        testMap.put(2,"2mA");
        testMap.put(3,"3mA");
        testMap.put(5,"5mA");
        testMap.put(8,"8mA");
        testMap.put(1,"1mA");
        testMap.put(7,"7mA");

        Set keySet = testMap.keySet();
        Iterator it = keySet.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " ");
        }

        List<String> values = new ArrayList<String>(testMap.values());
        System.out.println();
        for (int i=0;i<values.size();i++) {
            System.out.print(values.get(i) + " ");
        }


    }


}
