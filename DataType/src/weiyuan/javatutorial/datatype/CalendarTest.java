package weiyuan.javatutorial.datatype;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Weiyuan Zhu on 09/01/2017.
 * Java Calender tests
 * with SimpleDateFormat to display in string
 */
public final class CalendarTest {
    private CalendarTest() { }

    public static void main(final String[] args) {

        //system time, can be changed
        Calendar rightNow = Calendar.getInstance();

        System.out.println(rightNow.get(Calendar.DAY_OF_YEAR));
        System.out.println(rightNow.getTime());

        //Calender.JANUARY = 0, 1 = February
        Calendar expireDate = new GregorianCalendar(2017, Calendar.JANUARY, 1);
        System.out.println(expireDate.getTime());


        System.out.println(rightNow.before(expireDate));
        System.out.println(rightNow.after(expireDate));
    }
}
