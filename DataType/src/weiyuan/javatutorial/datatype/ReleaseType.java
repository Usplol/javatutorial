package weiyuan.javatutorial.datatype;

/**
 * Created by Weiyuan Zhu on 09/01/2017.
 * Java Enum Test
 */

public enum ReleaseType {
    Release("1.5.2"),
    BetaRelease("1.5.2 RC4"),
    Debug("1.5.2 debug");

    private String versionNumber;

    ReleaseType(final String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() {
        return this.versionNumber;
    }

    public static void main(final String[] args) {

        //enum values
        for (ReleaseType releaseType: ReleaseType.values()) {
            System.out.print(releaseType.ordinal() + " ");
            System.out.print(releaseType + " ");
            System.out.println(releaseType.getVersionNumber());
        }

        //switch
        ReleaseType rt = ReleaseType.Debug;
        switch (rt) {
            case Debug:
                System.out.println("Debug test");
            default: break;
        }
    }
}


