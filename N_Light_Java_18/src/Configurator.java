import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * To test CompletableFuture feature since Java 1.8
 * Concurrency, Socket, Executor
 */
public class Configurator {

    static NLight_Socket nLight = null;


    public static void main(String[] args) {
        Executor executor = Executors.newSingleThreadExecutor();
        Executor scheduledExecutor = Executors.newSingleThreadScheduledExecutor();

        nLight = new NLight_Socket();

        System.out.println("Main thread id: " + Thread.currentThread().getId());



        nLight.txFuture.orTimeout(25, TimeUnit.SECONDS).whenComplete((result,error)->{
            System.out.println("whenComplete thread id: " + Thread.currentThread().getId());


            if(error == null) {
                System.out.println(result);
            } else {
                System.out.println("Timeout");
                ((ScheduledExecutorService) scheduledExecutor).shutdown();
            }
            nLight.closeSocket();

        });

        ((ScheduledExecutorService) scheduledExecutor).scheduleAtFixedRate(()->{
            System.out.println("task issued");
        }, 1, 5, TimeUnit.SECONDS);
    }


    @Contract("_ -> param1")
    static List<Integer> addOneElement(List<Integer> buffer) {

        buffer.add(0x55);
        return buffer;

    }

    static void callback(List<Integer> buffer, Throwable t){

        System.out.println("rx callback");
        System.out.println("thread id: " + Thread.currentThread().getId());
        System.out.println(buffer);
        nLight.closeSocket();
    }
}
