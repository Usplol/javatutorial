import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class NLight_Socket {

    static String ip = "192.168.1.16";          // target N-Light ip address
    static final int port = 500;



    static final int UART_STOP_BIT_H = 0x5A;
    static final int UART_STOP_BIT_L = 0xA5;
    static final int UART_NEW_LINE_H = 0x0D;
    static final int UART_NEW_LINE_L = 0x0A;

    private Executor singleThreadExecutor = Executors.newSingleThreadExecutor();

    private Socket socket = null;
    private PrintWriter out = null;
    private InputStream in = null;

    private static char[] getOverAllStatus = new char[] {0x02,0xA0,0x32,0x29,0xD5,0x5A,0xA5};

    public  void connect() {
        try {
            //System.out.println("Start connection to " + ip + ": " + port);

            socket = new Socket(ip, port);
            socket.setSoTimeout(10);
            System.out.println("Connected to " + socket.getInetAddress() + ": " + socket.getPort());
            //socket.setSoTimeout(3 * 1000);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "ISO8859_1")), false);
            in = socket.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    final CompletableFuture<List<Integer>> txFuture = CompletableFuture.supplyAsync(()->
    {
        int bytes_received = 0;
        int len = -1;
        List<Integer> rxBuffer = new ArrayList<Integer>();

        try {

            if(socket == null || socket.isClosed()) {
                connect();
            }

            if(out != null) {
                out.println(getOverAllStatus);
                out.flush();
            }

            TimeUnit.SECONDS.sleep(1);


            while ((socket != null) && !socket.isClosed() && in.available() > 0) {
                int data = in.read();
                rxBuffer.add(data);
                if ((data == UART_NEW_LINE_L) && !rxBuffer.isEmpty() &&
                        (rxBuffer.get(rxBuffer.size() - 2) == UART_NEW_LINE_H) &&
                        (rxBuffer.get(rxBuffer.size() - 3) == UART_STOP_BIT_L) &&
                        (rxBuffer.get(rxBuffer.size() - 4) == UART_STOP_BIT_H))
                {
                    bytes_received += rxBuffer.size();

//                        for (int i = 0; i < rxBuffer.size(); i++) {
//                            System.out.print(String.format("%02X", rxBuffer.get(i)) + " ");
//                        }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rxBuffer;
    }, singleThreadExecutor);




    public void closeSocket() {
        if (socket!=null) {
            if(!socket.isClosed()) {
                try {
                    if(in !=null ) {
                        in.close();
                    }
                    if(out !=null) {
                        out.close();
                    }
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("socket closed");
    }

}
