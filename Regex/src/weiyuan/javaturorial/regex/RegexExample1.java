package weiyuan.javaturorial.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**}}{}
 * Created by Weiyuan Zhu on 04/01/2017.
 * Regex Character class (single char)
 */
public final class RegexExample1 {

    private RegexExample1() { }

    public static void main(final String[] args) {
        Pattern p = Pattern.compile(".s");
        Matcher m = p.matcher("as");
        boolean b = m.matches();

        if (m.find()) {
            System.out.println(m.start() + " " + m.end());
        }

        boolean b2 = Pattern.matches(".s", "ab");
        System.out.println(b + " " + b2);

        System.out.println(Pattern.matches(".[abc]", "ab"));
        System.out.println(Pattern.matches("[^abc]", "d"));
        System.out.println(Pattern.matches("[^abc]", "b"));
        System.out.println(Pattern.matches("[a-zA-Z]", "b"));
        System.out.println(Pattern.matches(".[a-zAB]", "BA"));
        System.out.println(Pattern.matches("[A-Z_0-9]", "A"));
    }

}
