package weiyuan.javaturorial.regex;

import java.util.regex.Pattern;

/**
 * Created by Weiyuan Zhu on 04/01/2017.
 * Regex char and quantifiers
 */
public final class RegexExample2 {

    private RegexExample2() { }

    public static void main(final String[] args) {
        System.out.println("? quantifier ...");
        System.out.println(Pattern.matches("[amn]?", "a"));
        System.out.println(Pattern.matches("[amn]?", "aaa"));
        System.out.println(Pattern.matches("[amn]?", "aammmnn"));
        System.out.println(Pattern.matches("[amn]?", "am"));
        System.out.println(Pattern.matches("[amn]?", "m"));

        System.out.println("+ quantifier ...");
        System.out.println(Pattern.matches("[amn]+", "m"));
        System.out.println(Pattern.matches("[amn]+", "aaammm"));
        System.out.println(Pattern.matches("[amn]+", "aaammmtz"));

        System.out.println("* quantifier ...");
        System.out.println(Pattern.matches("[amn]*", ""));
        System.out.println(Pattern.matches("[amn]*", "b"));
        System.out.println(Pattern.matches("[amn]*", "aamm"));
        System.out.println(Pattern.matches("[amn]*", "aammzz"));


    }
}
