package weiyuan.javaturorial.regex;

import java.util.regex.Pattern;

/**
 * Created by Weiyuan Zhu on 04/01/2017.
 * Regex MetaCharacters
 */
public final class RegexExample3 {

    private RegexExample3() { }

    public static void main(final String[] args) {
        System.out.println("meta characters ....");

        System.out.println(Pattern.matches("\\d", "9"));
        System.out.println(Pattern.matches("\\d+", "91212"));
        System.out.println(Pattern.matches("\\d{3}", "912"));
        System.out.println(Pattern.matches("\\d{3,}", "2"));

        System.out.println(Pattern.matches("\\D*", "jhjhh88"));
        System.out.println(Pattern.matches(".+\\s+$", "   4  "));



    }
}
