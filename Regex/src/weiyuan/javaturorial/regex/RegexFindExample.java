package weiyuan.javaturorial.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Weiyuan Zhu on 04/01/2017.
 */
public final class RegexFindExample {

    private RegexFindExample() { }


    public static void main(final String[] args) {
        Pattern pattern;
        Scanner keyboard = new Scanner(System.in);

        while (true) {
            System.out.println("Enter regex pattern:");
            String nextLine = keyboard.nextLine();

            if (nextLine.equals("exit")) {
                break;
            } else {
                pattern = Pattern.compile(nextLine);
            }

            System.out.println("Enter text:");
            Matcher matcher = pattern.matcher(keyboard.nextLine());
            boolean found = false;
            while (matcher.find()) {
                System.out.println("I found the text: "
                                    + "\"" + matcher.group() + "\""
                                    + " starting at index: " + matcher.start()
                                    + " and ending at index: " + matcher.end());
                found = true;
            }

            if (!found) {
                System.out.println("No match found.");
            }
        }
    }
}
