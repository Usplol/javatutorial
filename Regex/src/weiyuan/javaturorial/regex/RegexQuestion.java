package weiyuan.javaturorial.regex;

import java.util.regex.Pattern;

/**
 * Created by Weiyuan Zhu on 04/01/2017.
 */
public final class RegexQuestion {

    private RegexQuestion() { }

    public static void main(final String[] args) {
        //alpha numeric char only , 6 chars long only
        System.out.println(Pattern.matches("[a-zA-Z0-9]{6}", "123456"));

        //10 digit numeric chars, start with 7, 8, 9 only
        System.out.println(Pattern.matches("[789][0-9]{9}", "77345678909"));
        System.out.println(Pattern.matches("[789]\\d{9}", "77f34567890"));

    }
}
