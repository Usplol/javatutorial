public class BinarySearch {

    public static void main(String[] args) {
        int[] array = {8,8,8,8,8,8,8,8};
        int element = 8;

        int index = binarySearch_non_recursive(array, element);
        if (index == -1) {
            System.out.println("index = -1, element not found");
        } else if (array[index] == element) {       // double check search result is correct
            System.out.println(element + " found at index : " + index);
        } else {
            System.out.println("binarySearch ERROR");
        }

        int firstOcc = binarySearch_first_recursive(array, element, 0, array.length - 1);
        if (firstOcc == -1) {
            System.out.println("index = -1, element not found");
        } else if (array[firstOcc] == element) {       // double check search result is correct
            System.out.println(element + " first occurrence at index : " + firstOcc);
        } else {
            System.out.println("binarySearch ERROR");
        }

        int lastOcc = binarySearch_last_recursive(array, element, 0, array.length - 1);
        if (lastOcc == -1) {
            System.out.println("index = -1, element not found");
        } else if (array[lastOcc] == element) {       // double check search result is correct
            System.out.println(element + " last occurrence at index : " + lastOcc);
        } else {
            System.out.println("binarySearch ERROR");
        }

    }



    /**
     * Search an ordered array and return the index of element,
     * @param array an array which is in asc order
     * @param target number to search for
     * @return index of the target element, -1 for not found
     */

    public static int binarySearch_v1(int[] array, int target, int l, int r) {
        //element not in the array
        if(l > r) {
            return -1;
        }

        // get mid element in the search range
        int m = (l + r) / 2;
        int mid = array[m];

        // divide array in half and recursive search
        if (target == mid) {
            return m;
        } else if (target > array[m]) {
            return binarySearch_v1(array, target, m + 1, r);
        } else {    // <
            return binarySearch_v1(array, target, l, m - 1);
        }

    }

    /**
     *
     * @param array
     * @param target
     * @param l
     * @param r
     * @return  index of first occurrence of the element
     */
    public static int binarySearch_first_recursive(int[] array, int target, int l, int r) {
        //element not in the array
        if(l > r) {
            return -1;
        }

        // get mid element in the search range
        int m = (l + r) / 2;
        int mid = array[m];

        // divide array in half and recursive search
        if (target == mid) {
            int first;
            do {
                first = binarySearch_first_recursive(array, target, l, m - 1);
                if (first != -1) {
                    m = first;
                }
            } while (first != -1);

            return m;

        } else if (target > array[m]) {
            return binarySearch_first_recursive(array, target, m + 1, r);
        } else {    // <
            return binarySearch_first_recursive(array, target, l, m - 1);
        }
    }

    public static int binarySearch_last_recursive(int[] array, int target, int l, int r) {
        //element not in the array
        if(l > r) {
            return -1;
        }

        // get mid element in the search range
        int m = (l + r) / 2;
        int mid = array[m];

        // divide array in half and recursive search
        if (target == mid) {
            int last;
            do {
                last = binarySearch_first_recursive(array, target, m+1, r);
                if (last != -1) {
                    m = last;
                }
            } while (last != -1);

            return m;

        } else if (target > array[m]) {
            return binarySearch_last_recursive(array, target, m + 1, r);
        } else {    // <
            return binarySearch_last_recursive(array, target, l, m - 1);
        }
    }

    public static int binarySearch_non_recursive(int[] array, int target) {
        int l, r, m;
        l = 0;
        r = array.length - 1;

        int first = -1;

        while (l <= r) {
            m = (l + r) / 2;
            if (array[m] == target) {           // keep searching for first occurrence of the element
                first = m;
                r = m - 1;
            } else if (target > array[m]) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return first;
    }


}
