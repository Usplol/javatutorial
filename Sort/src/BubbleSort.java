public class BubbleSort {

    public static void BubbleSort_V1(int[] array) {
        int n = array.length;
        int temp;
        for (int i = n - 2; i >= 0; i-- ) {
            for (int j = i; j < n - 1; j++) {
                if (array[j] > array[j+1]) {            //swap if j > j+1
                    temp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
