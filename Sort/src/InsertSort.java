

public class InsertSort {

    public static void insertSortASC_V1(int[] array) {
        int temp;
        for (int i = 1;i < array.length; i++) {                                                                         // outer loop, check from second element to last element
            for (int j = 0; j < i; j++) {                                                                               // for element i, check all element before i (j < i)
                if (j == (i-1) ) {                                                                                      // last element before i (i-1)
                    if (array[i] < array[j]) {
                        temp = array[i];                                                                                // swap array[j] and array[i]
                        array[i] = array[j];
                        array[j] = temp;
                    } else {}                                                                                           // otherwise don' move array[i]
                } else {                                                                                                // other elements before i (0 -  (i -2))
                    if(array[i] < array[j]) {
                        temp = array[i];// array[i] < j, insert i before j
                        for(int k = i; k > j ;k--) {                                                                    // move each element back
                            array[k] = array[k-1];
                        }
                        array[j] = temp;
                        j = i;                                                                                          // jump out inner loop
                    } else if ((array[i] >= array[j]) && (array[i] < array[j+1])) {                                     // found position to insert i >= j and i <= i+1
                        temp = array[i];
                        for(int k = i; k > j + 1;k--) {                                                                 // move each element back
                            array[k] = array[k-1];
                        }
                        array[j+1] = temp;                                                                              // insert array[j+1]
                        j = i;                                                                                          // jump out inner loop
                    }
                }
            }
        }
    }

    public void insertSortASC_V2(int[] array) {
        for (int i = 1;i < array.length; i++) {                                                                         // outer loop, check from second element to last element
            int key = array[i];
            int j = i -1;
            for (; j >= 0; j--) {                                                                                       // for element i, check all element before i (j < i)
                if(array[j] > key) {
                    array[j+1] = array[j];
                } else {
                    break;
                }
            }
            array[j+1] = key;
        }
    }

    public static void insertSortASC_Break(int[] array) {                                                                 // 98 seconds for 1million integer

        for (int i = 1;i < array.length; i++) {                                                                         // outer loop, check from second element to last element
            int key = array[i];
            int j = i - 1;
            while (j>=0) {                                                                               // for element i, check all element before i (j < i)
                if (array[j] > key) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
                j--;
            }
            array[j+1] = key;

        }
    }


    /* This code is contributed by Rajat Mishra. */
    public static void insertSortASC_While(int[] array) {                                                              // 98 seconds for 1million integer

        int n = array.length;
        for (int i = 1;i < n; i++) {                                                                                    // outer loop, check from second element to last element
            int key = array[i];     // current element
            int j = i - 1;           //

            /* Move elements of arr[0..i-1], that are
               greater than key, to one position ahead
               of their current position */
            while ((j >= 0) && (array[j] > key)) {
                array[j+1] = array[j];
                j = j - 1;
            }
            array[j+1] = key;
        }
    }



}
