import java.util.ArrayList;

public class Main {
    private static int N = 10000000;

    public static void main(String[] args) {
        int n = 10000;
        ArrayList<int[]> testList = new ArrayList<>();
        int[] array = Utility.generateRandomIntegerArray(n);
        int[] array2 = new int[n];
        int[] array3 = new int[n];
        int[] array4 = new int[n];
        int[] array5 = new int[n];
        int[] array6 = new int[n];
        int[] array7 = new int[n];
        int[] array8 = new int[n];
        int[] array9 = new int[n];
        int[] array10 = new int[n];
        System.arraycopy(array, 0, array2, 0, array.length);
        System.arraycopy(array, 0, array3, 0, array.length);
        System.arraycopy(array, 0, array4, 0, array.length);
        System.arraycopy(array, 0, array5, 0, array.length);
        System.arraycopy(array, 0, array6, 0, array.length);
        System.arraycopy(array, 0, array7, 0, array.length);
        System.arraycopy(array, 0, array8, 0, array.length);
        System.arraycopy(array, 0, array9, 0, array.length);
        System.arraycopy(array, 0, array10, 0, array.length);
        testList.add(array);
        testList.add(array2);
        testList.add(array3);
        testList.add(array4);
        testList.add(array5);
        testList.add(array6);
        testList.add(array7);
        testList.add(array8);
        testList.add(array9);
        testList.add(array10);


        long startTime, elapsed;
        startTime = System.nanoTime();
        for (int i = 0;i<testList.size();i++) {
            //BubbleSort.BubbleSort_V1(testList.get(i));
            //SelectionSort.selectionSortAsc_v2(testList.get(i));
            InsertSort.insertSortASC_While(testList.get(i));
        }
        elapsed = System.nanoTime() - startTime;
        System.out.println(elapsed/10);

    }
}
