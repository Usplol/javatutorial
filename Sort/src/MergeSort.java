public class MergeSort {
    private static int N = 500000;

    public static void main(String[] args) {
        int[] array = Utility.generateRandomIntegerArray(N);
        System.out.println(Utility.isArraySorted(array, Utility.ASEC));
        //printArray(array, 0, array.length - 1);

        int l = 0;
        int r = array.length -1;
        long start, end;

        start = System.nanoTime();
        mergeSortArray(array, l, r);
        InsertSort.insertSortASC_While(array);
        end = System.nanoTime() - start;
        System.out.println("elapsed: " + end);
        System.out.println(Utility.isArraySorted(array, Utility.ASEC));
        //printArray(array, 0, array.length - 1);
    }


    private static void printArray(int[] array, int l, int r) {
        System.out.print("[");
        for (int i = l; i <=r; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }


    /**
     * merge sort for array ascending order recursively
     * @param l index for leftmost
     * @param r index for rightmost
     */
    public static void mergeSortArray(int[] array, int l, int r) {
        //printArray(array, l, r);
        if (l < r) {                                                                                                    // l < r means more than 1 elements in the array, need further divide
            int m = (l + r) / 2;
            mergeSortArray(array, l, m);
            mergeSortArray(array, m + 1, r);
            mergeArray(array, l, r, m);
        }                                                                                                               // else only 1 element in the array, return straight way

    }

    /**
     * Takes a array which has two parts, both parts are sorted,
     * Then merge two arrays into one sorted array
     * Takes N extra space and sorted array is put back in origin array
     * @param array [l, l+1, l+2 ... m, m+1, m+2 ... r]
     * @param l leftmost
     * @param r rightmost
     * @param m mid index to separate array into two
     */
    private static void mergeArray(int[] array, int l, int r, int m){
        int n1 = m - l + 1;                                                                                             // need extra N space to merge two arrays
        int n2 = r - m;
        int[] left = new int[n1];
        int[] right = new int[n2];
        System.arraycopy(array, l, left, 0, n1);                                                                // copy left sub array into left
        System.arraycopy(array, m + 1, right, 0, n2);                                                    // copy right sub array into right

        int k = l;                                                                                                      // k is the index write back into array
        int i = 0;                                                                                                      // left most index for array left and right
        int j = 0;
        while((i < n1) && (j < n2)) {
            if (left[i] < right[j]) {
                array[k] = left[i];
                i++;
            } else {
                array[k] = right[j];
                j++;
            }
            k++;
        }

        //write rest element in left and/or right back in array
        while (i < n1) {
            array[k] = left[i];
            k++;
            i++;
        }

        while (j < n2) {
            array[k] = right[j];
            k++;
            j++;
        }
    }




    /*
     * merge sort for linked list ascending order
     * recursive
     * l index for leftmost
     * r index for rightmost
     * m mid index to divide array
     */
    public static void mergeSortLinkedList(int[] array, int l, int r, int m) {

    }
}
