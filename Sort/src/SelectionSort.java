public class SelectionSort {

    /*
        The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.

        1) The subarray which is already sorted.
        2) Remaining subarray which is unsorted.

        In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray is picked and moved to the sorted subarray.
     */
    public static void selectionSortAsc_v1(int[] array) {

        int temp, minIndex;

        for (int i = 0; i < array.length; i++) {
            minIndex = i;
            //int min = array[j];
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    //min = array[i];
                    minIndex = j;
                }
            }
            temp = array[i];
            array[i]=array[minIndex];
            array[minIndex] = temp;
        }
    }

    public static void selectionSortAsc_v2(int[] array) {

        int temp, minIndex;

        for (int i = 0; i < array.length - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            temp = array[i];
            array[i]=array[minIndex];
            array[minIndex] = temp;
        }
    }
}
