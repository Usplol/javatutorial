import java.util.ArrayList;
import java.util.Random;

public class Utility {

    static public boolean ASEC = true;
    static public boolean DESC = false;


    static public int[] generateRandomIntegerArray(int n) {
        Random rand = new Random();
        int[] mArray = new int[n];
        for (int i = 0; i < n; i++) {
            mArray[i] = rand.nextInt(10000) + 1;
        }

        return mArray;
    }

    static boolean isArraySorted(int[] array, boolean order) {

        if (DESC == order) {
            for (int i = array.length - 1; i > 0; i--) {
                if (array[i] > array[i - 1]) {
                    return false;
                }
            }
        } else {
            for (int i = array.length - 1; i > 0; i--) {
                if (array[i] < array[i - 1]) {
                    return false;
                }
            }
        }


        return true;
    }

    static boolean isArraySortedRecursive(int[] array, int rightMostIndex) {
        if(rightMostIndex == 0) {
            return true;
        } else {
            boolean a = array[rightMostIndex] >= array[rightMostIndex - 1];
            if (a == false) {
                return false;
            }
            boolean b = isArraySortedRecursive(array, rightMostIndex - 1);
            if (b == false) {
                return false;
            }

            return a && b;
        }
    }
}
